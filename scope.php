<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>SCOPE JS</title>
	<script src="js.js" type="text/javascript" charset="utf-8" async defer></script>
</head>
<body>
	<script type="text/javascript">
		//contoh scope paramater global dan lokal

		var a = 10; //global

		function jumlah(x){ //x juga bersifat lokal hanya bisa digunakan fungsi itu sendiri
			var b = 40; //lokal
			document.write(x + b);
		}

		//jalankan fungsi jumlah
		jumlah(10);
		document.write(x);
	
	</script>
</body>
	<h1>BELAJAR FUNGSI RETURN JS</h1>
</html>