<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>KONDISIONAL ELSE IF JS</title>
	<script src="js.js" type="text/javascript" charset="utf-8" async defer></script>
</head>
<body>
	<script type="text/javascript">
		
		var x = 50; //tipe data boolean
		// var x = false; //tipe data boolean

		if(x > 5) {
			document.write('<b>x lebih Besar dari 5</b>'); //jika true akan tampilkan ini
		}else if (x >= 50) {
			document.write('<b>x sama dengan 50</b>'); //jika true akan tampilkan ini
		}
		else {
			document.write(' Tidak ada yang benar'); //jika false akan tampilkan ini
		}
	</script>
</body>
</html>

