<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>KONDISIONAL IF JS</title>
	<script src="js.js" type="text/javascript" charset="utf-8" async defer></script>
</head>
<body>
	<script type="text/javascript">
		
		var x = true; //tipe data boolean
		// var x = false; //tipe data boolean

		if(x) {
			document.write('<b>x Bernilai True</b>'); //jika true akan tampilkan ini
		}
		else {
			document.write(' x Bernilai False'); //jika false akan tampilkan ini
		}
	</script>
</body>
</html>

