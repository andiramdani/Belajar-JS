<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BELAJAR ARRAY JS</title>
	<script src="js.js" type="text/javascript" charset="utf-8" async defer></script>
</head>
<body>
	<script type="text/javascript">
		var x = ["andi" , "ramdani", "ok"];

		//pop, shift 
		//push, unshift

		// x.pop(); //hapus array urutan paling akhir
		// x.shift(); //hapus data array urutan paling awal
		// x.push("ikan"); //tambah data array di akhir
		// x.unshift("ikan"); //tamabah data array di awal

		// x[3] = "ikan enak"; // tambah data array urutan 3
		x[x.length] = "ikan enak"; // tambah data array jika data banyak

		document.write(x);
		
	</script>
</body>
</html>

