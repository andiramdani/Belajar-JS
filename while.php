<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Belajar Pengulangan While</title>
    
</head>
<body>

	<p>Click the button to loop through a block of code as long as i is less than 10.</p>

	<button onclick="myFunction()">Try it</button>
    <p id="demo"></p>

     <script>
       	function myFunction() {
		    var text = "";
		    var i = 0;
		    while (i < 10) {
		        text += "<br>The number is " + i;
		        i++;
		    }
		    document.getElementById("demo").innerHTML = text;
		}

    </script>
    
</body>
</html>
