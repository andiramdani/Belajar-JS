<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>METODE ARITMATIKA JS</title>
	<script src="js.js" type="text/javascript" charset="utf-8" async defer></script>
</head>
<body>
	<script type="text/javascript">
		// MIN , MAX , ROUND , CEIL, Floor
		var a = Math.random();
		var b = Math.max(2,5,6,7);
		//otomatis jika mendekat ke atas akan ke atas dan sebaliknya
		var c = Math.round(2.556); 
		var c1 = Math.ceil(2.556); // membulatkan ke angka atas
		var c2 = Math.floor(2.556); //membulatkan ke angka kecil

		document.write(c1 + ' ' + c2);
	</script>
</body>
	<h1>BELAJAR METODE ARITMATIK JS</h1>
</html>
