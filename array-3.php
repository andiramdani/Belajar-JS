<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BELAJAR GABUNGKAN DATA ARRAY JS</title>
	<script src="js.js" type="text/javascript" charset="utf-8" async defer></script>
</head>
<body>
	<script type="text/javascript">
		var x = ["andi" , "ramdani", "ok"];
		var y = ["a" , "b", "c"];

		// var gabung = x.concat(y); //gabungkan data array x dan y
		// x = x.slice(0,2)
		x.splice(1, 1, "ikan", "buaya");

		// x[1] = "kecoa"; //ganti nilai array ke 1;

		document.write(x);
		
	</script>
</body>
</html>

